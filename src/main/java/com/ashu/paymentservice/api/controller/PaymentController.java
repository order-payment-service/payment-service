package com.ashu.paymentservice.api.controller;

import com.ashu.paymentservice.api.entity.Payment;
import com.ashu.paymentservice.api.service.PaymentService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/payment")
public class PaymentController {
    @Autowired
    private PaymentService paymentService;

    @PostMapping("/doPayment")
    public Payment doPayment(@RequestBody Payment payment) throws JsonProcessingException {
        return paymentService.doPayment(payment);
    }

    @GetMapping("/{orderId}")
    public Payment paymentByOrderId(@PathVariable int orderId) throws JsonProcessingException {
        return paymentService.findByOrderId(orderId);
    }
}
