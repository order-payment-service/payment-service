package com.ashu.paymentservice.api.service;

import com.ashu.paymentservice.api.entity.Payment;
import com.ashu.paymentservice.api.repository.PaymentRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.UUID;

@Service
public class PaymentService {

    private final PaymentRepository paymentRepository;
    private Logger logger = LoggerFactory.getLogger(Payment.class);

    public PaymentService(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    public Payment doPayment(Payment payment) throws JsonProcessingException {
        logger.info("PaymentService : {}", new ObjectMapper().writeValueAsString(payment));
        payment.setTransactionId(UUID.randomUUID().toString());
        payment.setPaymentStatus(paymentProcessing());
        return paymentRepository.save(payment);
    }

    public String paymentProcessing(){
        return new Random().nextBoolean()?"success":"false";
    }

    public Payment findByOrderId(int orderId) throws JsonProcessingException {
        Payment payment = paymentRepository.findByOrderId(orderId);
        logger.info("PaymentService findByOrderId() : {}", new ObjectMapper().writeValueAsString(payment));
        return payment;
    }
}
